# Enable antigen
ANTIGEN_LOG=$HOME/.zsh/antigen/antigen.log
source $HOME/.zsh/antigen/antigen.zsh

# Enable Oh-My-Zsh
export ZSH=~/.oh-my-zsh
ZSH_THEME="robbyrussell"
plugins=(git)
source $ZSH/oh-my-zsh.sh

# Use a directory in home for the function path
fpath=( '$HOME/.zsh/functions' $fpath )
setopt prompt_subst

# Allow skip words using ctrl left + right
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# Enable antigen functions
antigen bundle git
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting

# Use special theme
antigen theme minimal

# Enable the pure theme
antigen bundle mafredri/zsh-async
antigen bundle sindresorhus/pure@main

if which crystal > /dev/null; then
    # Enable crystal tools
    antigen bundle veelenga/crystal-zsh
fi

# Tell antigen we've stopped config
antigen apply



# Add some awesome aliases
alias dotfiles="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
alias dotpackage-add="dotfiles submodule add"
alias dotpackage-remove="dotfiles submodule deinit"
alias dotpackage-list="dotfiles submodule status"
alias weather="wego"
alias shellconf="vim $HOME/.zshrc"
alias phplint="find . -name \"*.php\" -print0 | xargs -0 -n1 -P8 php -l > /dev/null"
alias ugh='sudo $(fc -ln -1)'
alias crap='sudo $(fc -ln -1)'
alias gcl='git branch --merged | grep -v \* | xargs git branch -D'
alias gcr='git branch -a --merged | grep -v \* | xargs git branch -D'
alias vim='GOENV_VERSION=$(go version | grep "[0-9]\.[0-9]*\.[0-9]*" -o1) vim'

function pomodoro () { termdown -bc 10 -t "Take a 5 minute break\!" "25m" }

# Nicely include secrets
if [ -d $HOME/.zsh/secrets ] && [ "$(ls -A $HOME/.zsh/secrets)" ]; then
	for file in $HOME/.zsh/secrets/*.secret; do
		. $file
	done
fi

if [ -d $HOME/.pyenv/bin ]; then
    export PATH="$HOME/.pyenv/bin:$PATH"
    if [ ! -d $HOME/.pyenv/plugins/pyenv-virtualenv ]; then
        git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
    fi
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    export PYTHON_CONFIGURE_OPTS="--enable-shared"
fi

if [ -d $HOME/.rbenv/bin ]; then
    export PATH="$HOME/.rbenv/bin:$PATH"
    eval "$(rbenv init -)"
    if [ ! -d "$(rbenv root)/plugins/ruby-build" ]; then
        git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
    fi
fi

if [ -d $HOME/.linkerd2/bin ]; then
    export PATH="$HOME/.linkerd2/bin:$PATH"
fi
if [ -f $HOME/.cargo/env ]; then
    source $HOME/.cargo/env
fi

# Allow bash style comments in cli
setopt interactivecomments
if [ -d $HOME/.local/bin ]; then
    export PATH=$PATH:$HOME/.local/bin
fi


if [ -d "$HOME/.nodenv/bin" ]; then
    export PATH="$HOME/.nodenv/bin:$PATH"
    eval "$(nodenv init -)"
    if [ ! -d "$(nodenv root)/plugins/node-build" ]; then
        mkdir -p "$(nodenv root)/plugins/node-build"
        git clone https://github.com/nodenv/node-build.git "$(nodenv root)"/plugins/node-build
    fi
    # TODO: Fix this not working?
    #if which yarn > /dev/null; then
    #    export PATH="$(yarn global bin):$PATH"
    #fi
    # NOTE: No longer works as npm removed the command
    #if which npm > /dev/null; then
    #    export PATH="$(npm bin --location=global):$PATH"
    #fi
fi

if which brew > /dev/null; then
    source $(brew --prefix php-version)/php-version.sh && php-version 7
fi

if [ -d "$HOME/.krew/bin" ]; then
    export PATH="${HOME}/.krew/bin:$PATH"
fi

if [ -d "/snap/bin" ]; then
    export PATH="/snap/bin:$PATH"
fi

if [ -d "$HOME/.phpenv/bin" ]; then
    export PATH="${HOME}/.phpenv/bin:$PATH"
    eval "$(phpenv init -)"
    if [ ! -d "$HOME/.phpenv/plugins/php-build" ]; then
        curl -L https://raw.githubusercontent.com/phpenv/phpenv-installer/master/bin/phpenv-installer | bash
    fi
fi

if which kubectl > /dev/null; then
    alias k="kubectl"
    alias kc="kubectx"
    alias ke="kubenv"
    alias ks="kustomize"
    if [ ! -d "$HOME/.krew/bin" ]; then
        (
          #set -x; cd "$(mktemp -d)" &&
          #curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.tar.gz" &&
          #tar zxvf krew.tar.gz &&
          #KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_$(uname -m | sed -e 's/x86_64/amd64/' -e 's/arm.*$/arm/')" &&
          #"$KREW" install krew
        )
    fi
    export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

    if [ -d "$HOME/.local/bin" ]; then
        if [ -f "$HOME/.local/bin/flux" ]; then
            . <(flux completion zsh)
        fi
        if [ ! -f "$HOME/.local/bin/kustomize" ]; then
            pushd "$(mktemp -d)"
            curl -fsSLO "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv4.0.3/kustomize_v4.0.3_linux_amd64.tar.gz"
            tar zxvf kustomize_v4.0.3_linux_amd64.tar.gz
            cp kustomize $HOME/.local/bin/
            popd
        fi
        if [ ! -f "$HOME/.local/bin/kubeval" ]; then
            pushd "$(mktemp -d)"
            curl -fsSLO "https://github.com/instrumenta/kubeval/releases/latest/download/kubeval-linux-amd64.tar.gz"
            tar xf kubeval-linux-amd64.tar.gz
            cp kubeval $HOME/.local/bin/
            popd
        fi
    fi
fi

if which aws-vault > /dev/null; then
    export AWS_VAULT_BACKEND=file;
fi

if [ -d "$HOME/.goenv" ]; then
    export GOENV_ROOT="$HOME/.goenv"
    export PATH="$HOME/.goenv/bin:$PATH"
    eval "$(goenv init -)"
    # Setup golang paths
    #export PATH="$GOROOT/bin:$PATH"
    #export PATH="$PATH:$GOPATH/bin"
fi
RPROMPT="%D{%y/%m/%f} | %@ %D{%Z}"
export TZ="Australia/Brisbane"

